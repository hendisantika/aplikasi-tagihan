create table bulk_delete_tagihan (
    id varchar(36),
    nomor_debitur varchar(255) not null,
    id_jenis_tagihan varchar(36) not null,
    delete_status varchar(100) not null,
    primary key (id),
    foreign key (id_jenis_tagihan) references jenis_tagihan(id)
);