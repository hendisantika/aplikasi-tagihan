package id.ac.tazkia.payment.virtualaccount.entity;

import lombok.Data;

import javax.persistence.*;

@Entity @Data
public class ResendPembayaran {
    @Id @Column(name = "id_pembayaran")
    private String id;

    @OneToOne @MapsId @JoinColumn(name = "id_pembayaran")
    private Pembayaran pembayaran;
}
